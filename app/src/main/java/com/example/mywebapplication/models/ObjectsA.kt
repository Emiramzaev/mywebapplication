package com.example.mywebapplication.models

import com.google.gson.annotations.SerializedName

data class ObjectsA(
    @SerializedName("id")var id: Int,
    @SerializedName("title")var title: String
)

