package com.example.mywebapplication.models

import com.google.gson.*
import java.lang.reflect.Type

class ObjectJsonSerializer : JsonDeserializer<DetailseBase> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): DetailseBase? {

        if (json == null) return null

        val jsonObj = json.asJsonObject

        return if (jsonObj.get("type").asString == "text") {
            DetaileText(
                jsonObj.get("contents").asString
            )
        } else {
            DetaileUrl(
                jsonObj.get("url").asString
            )
        }
    }
}