package com.example.mywebapplication.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.mywebapplication.R
import kotlinx.android.synthetic.main.fragment_text_view.*
import kotlinx.android.synthetic.main.fragment_web_view.*

class TextFragment : Fragment() {
    var contents: String = "hhhdddd"
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bundle = arguments
        if (bundle != null) {
            contents = bundle.getString("contents").toString()
        }
        return inflater.inflate(R.layout.fragment_text_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textViewContents.text = contents
    }
}