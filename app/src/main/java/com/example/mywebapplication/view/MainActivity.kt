package com.example.mywebapplication.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.mywebapplication.R
import com.example.mywebapplication.models.DetaileText
import com.example.mywebapplication.models.ObjectsA
import com.example.mywebapplication.models.DetaileUrl
import com.example.mywebapplication.models.DetailseBase
import com.example.mywebapplication.view.fragments.TextFragment
import com.example.mywebapplication.view.fragments.WebViewFragment
import com.example.mywebapplication.viewmodels.MainActivityViewModel
import com.example.mywebapplication.viewmodels.factories.ObjectsVMFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_web_view.*


class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: MainActivityViewModel
    var list = ArrayList<ObjectsA>()
    var currentObject: Int = 0
    lateinit var detailseBase: DetailseBase
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel =
            ViewModelProviders.of(this, ObjectsVMFactory()).get(MainActivityViewModel::class.java)

        viewModel.getObjectA().observe(this, Observer {
            list = it
            showDitalleObject()
        })
        furtherBtn.setOnClickListener {
          showDitalleObject()
        }


    }

    private fun showDitalleObject() {
        if (list.size == currentObject)
            currentObject = 0
        viewModel.getTextOrWebType(list[currentObject].id).observe(this, Observer {
            detailseBase = it
            when (it) {
                is DetaileUrl -> {
                    val args = Bundle()
                    args.putString("url",it.url)
                    createFragment(WebViewFragment(),args)
                }
                is DetaileText -> {
                    val args = Bundle()
                    args.putString("contents",it.text)
                    createFragment(TextFragment() , args)
                }
            }
        })
        currentObject++
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            super.onBackPressed()
        }
    }
   fun createFragment (fragment: Fragment , args: Bundle ){
       fragment.arguments = args
       supportFragmentManager.beginTransaction()
           .replace(R.id.flContainer, fragment)
           .commit()
   }

}
