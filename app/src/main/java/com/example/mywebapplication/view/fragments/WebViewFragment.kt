package com.example.mywebapplication.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_web_view.*
import android.webkit.WebResourceRequest
import android.os.Build
import android.annotation.TargetApi
import android.webkit.WebViewClient
import com.example.mywebapplication.R


class WebViewFragment : Fragment() {
    var url: String = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bundle = arguments
        if (bundle != null) {
            url = bundle.getString("url").toString()
        }

        return inflater.inflate(R.layout.fragment_web_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView.settings.javaScriptEnabled
        webView.webViewClient = MyWebViewClient()
        webView.loadUrl(url)
    }

    private inner class MyWebViewClient : WebViewClient() {
        @TargetApi(Build.VERSION_CODES.N)
        fun shouldOverrideUrlLoading(viewFragment: WebViewFragment, request: WebResourceRequest): Boolean {
            viewFragment.loadUrl(request.url.toString())
            return true
        }

        // Для старых устройств
        fun shouldOverrideUrlLoading(viewFragment: WebViewFragment, url: String): Boolean {
            viewFragment.loadUrl(url)
            return true
        }
    }

    private fun loadUrl(url: String) {

    }
}