package com.example.mywebapplication.repository

import com.example.mywebapplication.models.ObjectsA
import com.example.mywebapplication.models.DetailseBase
import com.example.mywebapplication.models.ObjectJsonSerializer
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.concurrent.TimeUnit

interface ApiService {

    companion object {

        private var retrofit: Retrofit? = null
        fun getRetrofitClient(): Retrofit {
            fun provideGson(): Gson =
                GsonBuilder()
                    .setLenient()
                    .registerTypeAdapter(DetailseBase::class.java,
                        ObjectJsonSerializer()
                    )
                    .create()

            fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(60, TimeUnit.SECONDS)
                .build()

            return Retrofit.Builder()
                .baseUrl("https://demo0040494.mockable.io/api/v1/")
                .addConverterFactory(GsonConverterFactory.create(provideGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient())
                .build()
        }
    }

    @GET("trending")
    fun getList(): Call<ArrayList<ObjectsA>>

    @GET("object/{id}")
    fun getTextOrWebType(
        @Path("id") i: Int
    ): Call<DetailseBase>
}