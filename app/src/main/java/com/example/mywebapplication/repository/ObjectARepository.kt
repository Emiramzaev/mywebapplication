package com.example.mywebapplication.repository

import com.example.mywebapplication.models.ObjectsA
import com.example.mywebapplication.models.DetaileUrl
import com.example.mywebapplication.models.DetailseBase
import retrofit2.Call

class ObjectARepository() {
    fun getObjectA(): Call<ArrayList<ObjectsA>>? {
        return ApiService.getRetrofitClient().create(ApiService::class.java).getList()
    }
    fun getTextOrWebType(int: Int): Call<DetailseBase>?{
        return ApiService.getRetrofitClient().create(ApiService::class.java).getTextOrWebType(int)
    }
}