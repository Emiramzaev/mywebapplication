package com.example.mywebapplication.viewmodels.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mywebapplication.repository.ObjectARepository
import com.example.mywebapplication.viewmodels.MainActivityViewModel

@Suppress("UNCHECKED_CAST")
class ObjectsVMFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainActivityViewModel(ObjectARepository()) as T
    }

}