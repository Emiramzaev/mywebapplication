package com.example.mywebapplication.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mywebapplication.models.ObjectsA
import com.example.mywebapplication.models.DetaileUrl
import com.example.mywebapplication.models.DetailseBase
import com.example.mywebapplication.repository.ObjectARepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivityViewModel(private val repository: ObjectARepository) : ViewModel() {
    fun getObjectA(): LiveData<ArrayList<ObjectsA>> {
        val result = MutableLiveData<ArrayList<ObjectsA>>()
        repository.getObjectA()?.enqueue(object : Callback<ArrayList<ObjectsA>> {
            override fun onResponse(
                call: Call<ArrayList<ObjectsA>>,
                response: Response<ArrayList<ObjectsA>>
            ) {
                if (response.isSuccessful) {
                    result.value = response.body()
                }
            }

            override fun onFailure(
                call: Call<ArrayList<ObjectsA>>,
                t: Throwable) {

            }
        })
        return result
    }

    fun getTextOrWebType(id: Int): LiveData<DetailseBase> {
        val result = MutableLiveData<DetailseBase>()
        repository.getTextOrWebType(id)?.enqueue(object : Callback<DetailseBase> {
            override fun onResponse(
                call: Call<DetailseBase>,
                response: Response<DetailseBase>
            ) {
                result.value = response.body()
            }

            override fun onFailure(call: Call<DetailseBase>, t: Throwable) {

            }
        })
        return result
    }
}
